#!/usr/bin/env bash

[[ -d /mnt/boot ]] && umount /mnt/boot
[[ -d /mnt ]] && umount /mnt

[[ -b /dev/mapper/Container ]] && vgremove --force Container
[[ -b /dev/mapper/cryptroot ]] && cryptsetup luksClose cryptroot

dd if=/dev/zero of=/dev/vdb bs=1M count=10
