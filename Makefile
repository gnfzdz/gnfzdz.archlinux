##
# Ansible Collection: gnfzdz.archlinux
#
# @file
# @version 0.1

.DEFAULT_GOAL := help

COLLECTION_NAME := archlinux
LINKED_COLLECTION_DIR := "$${HOME}/.ansible/collections/ansible_collections/gnfzdz/${COLLECTION_NAME}"
REAL_COLLECTION_DIR := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

build: version
	@echo "Generating release artifact for gnfzdz.$(COLLECTION_NAME) / $(VERSION_CURRENT)"
	@mkdir -p ./build
	@poetry run ansible-galaxy collection build --output-path ./build
.PHONY: build

init: ## Initialize the project
	@echo "Initialize project"
	@poetry install
	@echo "End of initialization"
.PHONY: init

$(LINKED_COLLECTION_DIR):
	@echo "Symlinking collection into search path for local development"
	@mkdir -p "${HOME}/.ansible/collections/ansible_collections/gnfzdz"
	@ln -s "${REAL_COLLECTION_DIR}" "${LINKED_COLLECTION_DIR}"
	@echo "Symlink created successfully"

link: | $(LINKED_COLLECTION_DIR) # Symlink this directory into search path for local development
.PHONY: link

lint:
	@poetry run ansible-lint -v
	@poetry run yamllint .
.PHONY: lint

release: build version-tag version-increment
.PHONY: release

test: lint test-quick test-default test-install
.PHONY: test

test-default:
	@poetry run molecule test
.PHONY: test-default

test-quick:
	@poetry run molecule test -s quick
.PHONY: test-quick

test-install:
	@poetry run molecule test -s install
.PHONY: test-install

version:
	$(eval VERSION_CURRENT := $(shell grep "version:" galaxy.yml | sed 's/^.*:\s*//' | sed 's/\s*$$//'))
	$(eval VERSION_PIECES := $(subst ., , $(VERSION_CURRENT)))
	$(eval VERSION_NEXT := $(word 1, $(VERSION_PIECES)).$(word 2, $(VERSION_PIECES)).$(shell echo $$(( $(word 3, $(VERSION_PIECES))+1 ))))
.PHONY: version

version-tag: version
	@echo "Tagging current version: '$(VERSION_CURRENT)'"
	@echo "Validating repository has no uncomitted changes"
	@git diff --exit-code > /dev/null
	@git tag -a $(VERSION_CURRENT) -m 'Published version $(VERSION_CURRENT) of collection gnfzdz.$(COLLECTION_NAME)'
	@git push --follow-tags
.PHONY: version-tag

version-increment: version
	@echo "Preparing next version: '$(VERSION_NEXT)'"
	@echo "Validating repository has no uncomitted changes"
	@git diff --exit-code > /dev/null
	@sed -i'' 's/^version:.*/version: $(VERSION_NEXT)/' galaxy.yml
	@sed -i'' 's/^version\s*= .*/version = "$(VERSION_NEXT)"/' pyproject.toml
	@git commit -am "Bump version to $(VERSION_NEXT) in preparation for next release"
.PHONY: version-increment

help: ## Show this help message
	@echo ''
	@echo 'Usage:'
	@echo '  make <target>'
	@echo ''
	@echo 'Targets:'
	@echo '  help			Show this help message'
	@echo '  init			Initialize the project by downloading all required dependencies'
	@echo '  link			Symlink this project into the ansible collection search path (for local development)'
	@echo '  lint			Lint the project using ansible-lint and yamllint'
	@echo '  release		Build a new release artifact, tag the current commit with the version and bump version for next release iteration'
	@echo '  test			Execute all repository quality tests (both linting and molecule scenarios)'
	@echo '  test-quick		Execute a full test of the collection in a local container'
	@echo '  test-default	Execute a more complete test of the collection\'s roles using a virtual machine'
	@echo '  test-install	Execute a test using the install role to partition and minimally configure a new archlinux system'
.PHONY: help
# end
